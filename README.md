# Vk Sample Toy
This project aims to provide visual debugging for the rasterization behaviour
of a Vulkan implementation.

Users can place geometry on a small raster grid, blown up to show how the
rasterizer is determining which pixels are covered by the geometry on a pixel-
by-pixel basis.