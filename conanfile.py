from conans import ConanFile


class VkSampleToyConanFile(ConanFile):
    name = "vk-sample-toy"
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake_find_package"
    requires = (
        'lmlib/0.0.1',
        "lmgl/0.0.1",
        "lmpl/0.0.1",
        'fmt/5.3.0@bincrafters/stable',
        'embed-resource/0.1@lawrencem/stable',
        'freetype/2.9.1@bincrafters/stable',
        'boost/1.69.0@conan/stable',
    )

    def imports(self):
        self.copy("embed-resource", src="bin")
        self.copy("embed-resource.exe*", src="bin")
        self.copy('embed-resource.cmake', dst='scripts', src='cmake')
        self.copy('glslangValidator*', src='bin')
