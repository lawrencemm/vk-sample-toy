#include <lmgl/lmgl.h>
#include <lmlib/variant_visitor.h>
#include <lmpl/lmpl.h>

class vk_sample_toy_app
{
  public:
    vk_sample_toy_app()
        : renderer{lmgl::create_renderer({})},
          display{lmpl::create_display()},
          window_size{
            display->get_primary_screen()->get_size().proportion({1, 2})},
          window{display->create_window(lmpl::window_init{window_size})}
    {
    }

    void render()
    {
        auto frame = stage->wait_for_frame();
        frame->clear_colour({1.f, 1.f, 1.f, 1.f});
        frame->build();
        frame->submit();
        frame->wait_complete();
    }

    void main()
    {
        window->show();
        stage = renderer->create_stage(lmgl::stage_init{window.get()});

        bool quit{false};
        while (!quit)
        {
            auto msg = display->wait_for_message();
            msg >> lm::variant_visitor{
                     [&](lmpl::close_message) { quit = true; },
                     [&](lmpl::repaint_message) { render(); },
                     [](auto) {},
                   };
        }
    }

    lmgl::renderer renderer;
    lmpl::display display;
    lm::size2i window_size;
    lmpl::window window;
    lmgl::stage stage;
};

int main()
{
    auto app = vk_sample_toy_app{};
    app.main();
    return 0;
}
